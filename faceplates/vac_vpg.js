importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

//This script will perform all the animations of this window

//Get the PVs
var pvManMode = PVUtil.getDouble(pvs[0]);
var pvOff = PVUtil.getDouble(pvs[1]);
var pvOn = PVUtil.getDouble(pvs[2]);
var pvWarning = PVUtil.getDouble(pvs[3]);
var pvError = PVUtil.getDouble(pvs[4]);
var pvWarningCode = PVUtil.getDouble(pvs[5]);
var pvErrorCode = PVUtil.getDouble(pvs[6]);
var pvMode = PVUtil.getDouble(pvs[7]);
var pvModeSP = PVUtil.getDouble(pvs[8]);

//Get all widgets
var wgStatus = widget.getWidget("Status");
var wgManMode = widget.getWidget("ManMode");
var wgErrorCode = widget.getWidget("ErrorCode");
var wgWarningCode = widget.getWidget("WarningCode");
var wgMode = widget.getWidget("Mode");
var wgCurrentStep = widget.getWidget("CurrentStep");
var wgModeSP = widget.getWidget("ModeSP");
var pumpingGroup = widget.getPropertyValue("name");

//Animate widgets

//ManMode
if (pvManMode == 0)
	wgManMode.setPropertyValue("text","Auto");
else
	wgManMode.setPropertyValue("text","Manual");

//Status
if (pvOff == 1 && pvOn == 0)
	wgStatus.setPropertyValue("text","Stopped");
else if (pvOff == 0 && pvOn == 1)
	wgStatus.setPropertyValue("text","Started");
else if (pvOff == 1 && pvOn == 1)
	wgStatus.setPropertyValue("text","Invalid");
else if (pvOff == 0 && pvOn == 0)
	wgStatus.setPropertyValue("text","Unknown");
else
	wgStatus.setPropertyValue("text","Status");

//Error Code
switch (pvErrorCode) {
	case 99:
		wgErrorCode.setPropertyValue("text", "Hardware Interlock");
		break;
	case 98:
		wgErrorCode.setPropertyValue("text", "Software Interlock");
		break;
	case 97:
		wgErrorCode.setPropertyValue("text", "All VPTs / VVAs Locked");
		break;
	case 96:
		wgErrorCode.setPropertyValue("text", "Power Supply Error");
		break;
	case 95:
		wgErrorCode.setPropertyValue("text", "High Vacuum Manifold Vented Pressure Interlock");
		break;
	case 94:
		wgErrorCode.setPropertyValue("text", "Low Vacuum Manifold Vented Pressure Interlock");
		break;
	case 93:
		wgErrorCode.setPropertyValue("text", "Vacuum Sector Vented Pressure Interlock");
		break;
	case 51:
		wgErrorCode.setPropertyValue("text", "Primary Pump Error");
		break;
	default: 
		wgErrorCode.setPropertyValue("text", "");
}

//Warning Code
switch (pvWarningCode) {
	case 99:
		wgWarningCode.setPropertyValue("text", "Backing System #1: Primary Pump #1 Error Status");
		break;
	case 98:
		wgWarningCode.setPropertyValue("text", "Backing System #1: Angle Valve Error Status");
		break;
	case 97:
		wgWarningCode.setPropertyValue("text", "Rescue Backing System: Angle Valve Error Status");
		break;
	case 96:
		wgWarningCode.setPropertyValue("text", "Rescue Backing System: Primary Pump Error Status");
		break;
	case 95:
		wgWarningCode.setPropertyValue("text", "Rescue Backing System: Bypass Valve: Error Status");
		break;
	case 94:
		wgWarningCode.setPropertyValue("text", "High Vacuum Pumping System #1: Turbo-Molecular Pump #1 Error Status");
		break;
	case 93:
		wgWarningCode.setPropertyValue("text", "High Vacuum Pumping System #1: Vacuum Valve #1 Error Status");
		break;
	case 92:
		wgWarningCode.setPropertyValue("text", "High Vacuum Pumping System #2: Turbo-Molecular Pump #1 Error Status");
		break;
	case 91:
		wgWarningCode.setPropertyValue("text", "High Vacuum Pumping System #2: Vacuum Valve #1 Error Status");
		break;
	case 90:
		wgWarningCode.setPropertyValue("text", "Beam Vacuum: Gas Injection System #1 Insulation Valve Error Status");
		break;
	case 49:
		wgWarningCode.setPropertyValue("text", "Backing System #1: Primary Pump #1 Blocked Off Status");
		break;
	case 48:
		wgWarningCode.setPropertyValue("text", "Backing System #1: Angle Valve Blocked Close Status");
		break;
	case 47:
		wgWarningCode.setPropertyValue("text", "High Vacuum Pumping System #1: Turbo-Molecular Pump #1 Blocked Off Status");
		break;
	case 46:
		wgWarningCode.setPropertyValue("text", "High Vacuum Pumping System #1: Vacuum Valve #1 Blocked Close Status");
		break;
	case 45:
		wgWarningCode.setPropertyValue("text", "High Vacuum Pumping System #2: Turbo-Molecular Pump #1 Blocked Off Status");
		break;
	case 44:
		wgWarningCode.setPropertyValue("text", "High Vacuum Pumping System #2: Vacuum Valve #1 Blocked Close Status");
		break;
	case 43:
		wgWarningCode.setPropertyValue("text", "Rescue Backing System: Bypass Valve: Blocked Close Status");
		break;
	case 42:
		wgWarningCode.setPropertyValue("text", "Rescue Backing System: Primary Pump Blocked Off Status");
		break;
	case 41:
		wgWarningCode.setPropertyValue("text", "Rescue Backing System: Angle Valve Blocked Close Status");
		break;
}

if (pumpingGroup == "LNS-LEBT-010:VAC-VPG-11010") {
	switch (pvWarningCode) {
		case 1:
			wgWarningCode.setPropertyValue("text", "Primary Pump Error (VPDP-00031)");
			break;
		case 2:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-00031)");
			break;
		case 3:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-00041)");
			break;
		case 4:
			wgWarningCode.setPropertyValue("text", "Rescue Pump Error (VPDP-00071)");
			break;
		case 5:
			wgWarningCode.setPropertyValue("text", "Turbo-Pump Error (VPT-02100)");
			break;
		case 6:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-02100)");
			break;
		case 7:
			wgWarningCode.setPropertyValue("text", "Turbo-Pump Error (VPT-03100)");
			break;
		case 8:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-03100)");
			break;
		case 9:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-01100)");
			break;
		case 10:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-04100)");
			break;
		case 11:
			wgWarningCode.setPropertyValue("text", "All Devices Are Locked");
			break;
		case 12:
			wgWarningCode.setPropertyValue("text", "Manual Valve Not Closed");
			break;
		case 13:
			wgWarningCode.setPropertyValue("text", "VVA-00031 Locked");
			break;
		case 14:
			wgWarningCode.setPropertyValue("text", "VVA-02100 Locked");
			break;
		case 15:
			wgWarningCode.setPropertyValue("text", "VPT-02100 Locked");
			break;
		case 16:
			wgWarningCode.setPropertyValue("text", "VVA-03100 Locked");
			break;
		case 17:
			wgWarningCode.setPropertyValue("text", "VPT-03100 Locked");
			break;
		case 18:
			wgWarningCode.setPropertyValue("text", "Gas Injection Valve Locked");
			break;
		case 19:
			wgWarningCode.setPropertyValue("text", "Rescue Pump Locked");
			break;
		case 20:
			wgWarningCode.setPropertyValue("text", "Rescue Valve Locked");
			break;

		default: 
			wgWarningCode.setPropertyValue("text", "");
	}
} else if (pumpingGroup == "LNS-LEBT-010:VAC-VPG-22020") {
	switch (pvWarningCode) {
		case 1:
			wgWarningCode.setPropertyValue("text", "Primary Pump Error (VPDP-00071)");
			break;
		case 2:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-00071)");
			break;
		case 3:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-00041)");
			break;
		case 4:
			wgWarningCode.setPropertyValue("text", "Rescue Pump Error (VPDP-00031)");
			break;
		case 5:
			wgWarningCode.setPropertyValue("text", "Turbo-Pump Error (VPT-06100)");
			break;
		case 6:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-06100)");
			break;
		case 7:
			wgWarningCode.setPropertyValue("text", "Turbo-Pump Error (VPT-07100)");
			break;
		case 8:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-07100)");
			break;
		case 9:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-01100)");
			break;
		case 10:
			wgWarningCode.setPropertyValue("text", "Valve Error (VVA-04100)");
			break;
		case 11:
			wgWarningCode.setPropertyValue("text", "All Devices Are Locked");
			break;
		case 12:
			wgWarningCode.setPropertyValue("text", "Manual Valve Not Closed");
			break;
		case 13:
			wgWarningCode.setPropertyValue("text", "VVA-00071 Locked");
			break;
		case 14:
			wgWarningCode.setPropertyValue("text", "VVA-06100 Locked");
			break;
		case 15:
			wgWarningCode.setPropertyValue("text", "VPT-06100 Locked");
			break;
		case 16:
			wgWarningCode.setPropertyValue("text", "VVA-07100 Locked");
			break;
		case 17:
			wgWarningCode.setPropertyValue("text", "VPT-07100 Locked");
			break;
		case 18:
			wgWarningCode.setPropertyValue("text", "Gas Injection Valve Locked");
			break;
		case 19:
			wgWarningCode.setPropertyValue("text", "Rescue Pump Locked");
			break;
		case 20:
			wgWarningCode.setPropertyValue("text", "Rescue Valve Locked");
			break;

		default: 
			wgWarningCode.setPropertyValue("text", "");
	}
}

//Mode
switch (pvMode) {
	case 0:
		wgMode.setPropertyValue("text", "0 - Not Used");
		break;
	case 1:
		wgMode.setPropertyValue("text", "1 - Normal Pumping Mode");
		break;
	case 3:
		wgMode.setPropertyValue("text", "3 - Pump-Down Mode");
		break;
	case 5:
		wgMode.setPropertyValue("text", "5 - Leak Detection Mode");
		break;
	case 6:
		wgMode.setPropertyValue("text", "6 - Leak Detection - Partial Flow Mode");
		break;
	case 7:
		wgMode.setPropertyValue("text", "7 - Leak Detection - Manual Mode");
		break;
	case 8:
		wgMode.setPropertyValue("text", "8 - Venting Mode");
		break;
	case 10:
		wgMode.setPropertyValue("text", "10 - Manual Mode");
		break;
	default: 
		wgMode.setPropertyValue("text", "");
}

//ModeSP
switch (pvModeSP) {
	case 0:
		wgModeSP.setPropertyValue("label", "0 - Not Used");
		break;
	case 1:
		wgModeSP.setPropertyValue("label", "1 - Normal Pumping Mode");
		break;
	case 3:
		wgModeSP.setPropertyValue("label", "3 - Pump-Down Mode");
		break;
	case 5:
		wgModeSP.setPropertyValue("label", "5 - Leak Detection Mode");
		break;
	case 6:
		wgModeSP.setPropertyValue("label", "6 - Leak Detection - Partial Flow Mode");
		break;
	case 7:
		wgModeSP.setPropertyValue("label", "7 - Leak Detection - Manual Mode");
		break;
	case 8:
		wgModeSP.setPropertyValue("label", "8 - Venting Mode");
		break;
	case 10:
		wgModeSP.setPropertyValue("label", "10 - Manual Mode");
		break;
	default: 
		wgModeSP.setPropertyValue("label", "");
}

