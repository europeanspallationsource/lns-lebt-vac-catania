importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

//This script will perform all the animations of this window

//Get the PVs
var pvUndefined = PVUtil.getDouble(pvs[0]);
var pvOpen = PVUtil.getDouble(pvs[1]);
var pvClosed = PVUtil.getDouble(pvs[2]);
var pvError = PVUtil.getDouble(pvs[3]);
var pvManMode = PVUtil.getDouble(pvs[4]);

//Get all widgets
var wgBody = widget.getWidget("Body");
var wgManMode = widget.getWidget("ManMode");

//Animate widgets

//Mode
if (pvManMode == 0)
	wgManMode.setPropertyValue("visible", false);
else {
	wgManMode.setPropertyValue("text", "MANUAL");
	wgManMode.setPropertyValue("visible", true);
}

//Status
if (pvError == 1)
	wgBody.setPropertyValue("background_color", "Red On");
else if (pvClosed == 1 && pvOpen == 0)
	wgBody.setPropertyValue("background_color", "White");
else if (pvOpen == 1 && pvClosed == 0)
	wgBody.setPropertyValue("background_color", "Green On");
else if (pvUndefined == 1 || (pvClosed == 0 && pvOpen == 0))
	wgBody.setPropertyValue("background_color", "Blue");
else
	wgBody.setPropertyValue("background_color", "GUI Background Grey");

