importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

//This script will perform all the animations of this window

//Get the PVs
var pvOff = PVUtil.getDouble(pvs[0]);
var pvAccelerating = PVUtil.getDouble(pvs[1]);
var pvNominalSpeed = PVUtil.getDouble(pvs[2]);
var pvError = PVUtil.getDouble(pvs[3]);

//Get all widgets
var wgBody = widget.getWidget("Body");

//Animate widgets

//Status
if (pvError == 1)
	wgBody.setPropertyValue("image_file","../images/vac-vpt-error.png");
else if (pvAccelerating == 1)
	wgBody.setPropertyValue("image_file","../images/vac-vpt-accel.png");
else if (pvNominalSpeed == 1)
	wgBody.setPropertyValue("image_file","../images/vac-vpt-nominal.png");
else
	wgBody.setPropertyValue("image_file","../images/vac-vpt-off.png");

