importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

//This script will perform all the animations of this window

//Get the PVs
var pvManMode = PVUtil.getDouble(pvs[0]);
var pvWarning = PVUtil.getDouble(pvs[1]);
var pvError = PVUtil.getDouble(pvs[2]);
var pvIntlHWHealthy = PVUtil.getDouble(pvs[3]);
var pvIntlPressHealthy = PVUtil.getDouble(pvs[4]);
var pvIntlHWTripped = PVUtil.getDouble(pvs[5]);
var pvIntlPressTripped = PVUtil.getDouble(pvs[6]);
var pvIntlHWOverriden = PVUtil.getDouble(pvs[7]);
var pvIntlPressOverriden = PVUtil.getDouble(pvs[8]);
var pvIntlHWNotused = PVUtil.getDouble(pvs[9]);
var pvIntlPressNotused = PVUtil.getDouble(pvs[10]);

//Get all widgets
var wgManMode = widget.getWidget("ManMode");
var wgIntlkHW = widget.getWidget("Intlk:HW");
var wgIntlkPress = widget.getWidget("Intlk:Pressure");
var wgWarningError = widget.getWidget("WarningError");

//Animate widgets

//ManMode
if (pvManMode==1) {
	wgManMode.setPropertyValue("visible",true);
        wgManMode.setPropertyValue("text", "MANUAL");
}
else if (0) {
	wgManMode.setPropertyValue("visible",true);
        wgManMode.setPropertyValue("text", "STARTED");
}
else if (0) {
	wgManMode.setPropertyValue("visible",true);
        wgManMode.setPropertyValue("text", "LOCKED");
}
else
	wgManMode.setPropertyValue("visible",false);

//Intlk
if (pvIntlHWNotused == 1)
	wgIntlkHW.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(255, 255, 255));
else if (pvIntlHWOverriden == 1)
	wgIntlkHW.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(255, 255, 0));
else if (pvIntlHWTripped == 1)
	wgIntlkHW.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(255, 0, 0));
else if (pvIntlHWHealthy == 1)
	wgIntlkHW.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(0, 255, 0));
else
	wgIntlkHW.setPropertyValue("background_color", "GUI Background Grey");

if (pvIntlPressNotused == 1)
	wgIntlkPress.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(255, 255, 255));
else if (pvIntlPressOverriden == 1)
	wgIntlkPress.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(255, 255, 0));
else if (pvIntlPressTripped == 1)
	wgIntlkPress.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(255, 0, 0));
else if (pvIntlPressHealthy == 1)
	wgIntlkPress.setPropertyValue("background_color", ColorFontUtil.getColorFromRGB(0, 255, 0));
else
	wgIntlkPress.setPropertyValue("background_color", "GUI Background Grey");


//WarningError
if (pvWarning==1 && pvError==1) {
	wgWarningError.setPropertyValue("visible",true);
	wgWarningError.setPropertyValue("border_color","Major");
}
else if (pvWarning==0 && pvError==1) {
	wgWarningError.setPropertyValue("visible",true);
	wgWarningError.setPropertyValue("border_color","Major");
}
else if (pvWarning==1 && pvError==0) {
	wgWarningError.setPropertyValue("visible",true);
	wgWarningError.setPropertyValue("border_color","Minor");
}
else
	wgWarningError.setPropertyValue("visible",false);
