importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

//This script will perform all the animations of this window

//Get the PVs
var pvWgName      = PVUtil.getString(pvs[0]);
var pvIntlHealthy = PVUtil.getDouble(pvs[1]);
var pvIntlTripped = PVUtil.getDouble(pvs[2]);
var pvIntlOverriden = PVUtil.getDouble(pvs[3]);
var pvIntlNotused = PVUtil.getDouble(pvs[4]);

//Get all widgets
var wgIntlk = widget.getWidget(pvWgName);

//Animate widgets
//Interlock
if (pvIntlNotused == 1)
	wgIntlk.setPropertyValue("background_color", "White");
else if (pvIntlOverriden == 1)
	wgIntlk.setPropertyValue("background_color", "Yellow");
else if (pvIntlTripped == 1)
	wgIntlk.setPropertyValue("background_color", "Red On");
else if (pvIntlHealthy == 1)
	wgIntlk.setPropertyValue("background_color", "Green On");
else
	wgIntlk.setPropertyValue("background_color", "GUI Background Grey");
