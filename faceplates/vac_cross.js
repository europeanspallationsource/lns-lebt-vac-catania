importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

//This script will perform all the animations of this window

//Get the PVs
var pvBlocked = PVUtil.getDouble(pvs[0]);

//Animate widgets

//Blocked
if (pvBlocked == 1)
	widget.setPropertyValue("visible", "true");
else
	widget.setPropertyValue("visible", "false");

