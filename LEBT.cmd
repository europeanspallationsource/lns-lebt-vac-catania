epicsEnvSet(PLCNAME, "LNS-LEBT-010:VAC-PLC-11111")
epicsEnvSet(IPADDR, "10.4.3.101")
epicsEnvSet(S7DRVPORT, "2000")
epicsEnvSet(MODBUSDRVPORT, "502")
epicsEnvSet(INSIZE, "2000")
epicsEnvSet(RECVTIMEOUT, "1000")
require plc_lns_lebt_010_vac_plc_11111, catania
< /opt/epics/modules/plc_lns_lebt_010_vac_plc_11111/catania/startup/startup.cmd





epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(IPADDR, "10.4.0.213")
epicsEnvSet(PORT, "4001")
require vac_ctrl_mks946_937b, catania
< /opt/epics/modules/vac_ctrl_mks946_937b/catania/startup/vac_ctrl_mks946_937b_ethernet.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGP-00021")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "1")
epicsEnvSet(CHANNEL_C, "1")
epicsEnvSet(RELAY1, "1")
epicsEnvSet(RELAY2, "2")
require vac_gauge_mks_vgp, catania
< /opt/epics/modules/vac_gauge_mks_vgp/catania/startup/vac_gauge_mks_vgp.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGP-00031")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "2")
epicsEnvSet(CHANNEL_C, "2")
epicsEnvSet(RELAY1, "3")
epicsEnvSet(RELAY2, "4")
require vac_gauge_mks_vgp, catania
< /opt/epics/modules/vac_gauge_mks_vgp/catania/startup/vac_gauge_mks_vgp.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGC-10000")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "3")
epicsEnvSet(CHANNEL_C, "3")
epicsEnvSet(RELAY1, "5")
epicsEnvSet(RELAY2, "6")
epicsEnvSet(RELAY3, "7")
epicsEnvSet(RELAY4, "8")
require vac_gauge_mks_vgc, catania
< /opt/epics/modules/vac_gauge_mks_vgc/catania/startup/vac_gauge_mks_vgc.cmd





epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(IPADDR, "10.4.0.213")
epicsEnvSet(PORT, "4004")
require vac_ctrl_mks946_937b, catania
< /opt/epics/modules/vac_ctrl_mks946_937b/catania/startup/vac_ctrl_mks946_937b_ethernet.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VVMC-01100")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(CHANNEL, "5")
require vac_mfc_mks_gv50a, catania
< /opt/epics/modules/vac_mfc_mks_gv50a/catania/startup/vac_mfc_mks_gv50a.cmd

#epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGD-10000")
#epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEVMC-01100")
#epicsEnvSet(CHANNEL, "3")
#epicsEnvSet(CHANNEL_C, "3")
#epicsEnvSet(RELAY1, "5")
#epicsEnvSet(RELAY2, "6")
#require vac_gauge_mks_vgd, catania
#< /opt/epics/modules/vac_gauge_mks_vgd/catania/startup/vac_gauge_mks_vgd.cmd





epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VEPT-02100")
epicsEnvSet(IPADDR, "10.4.0.213")
epicsEnvSet(PORT, "4002")
require vac_ctrl_leyboldtd20, catania
< /opt/epics/modules/vac_ctrl_leyboldtd20/catania/startup/vac_ctrl_leyboldtd20_ethernet.cmd





epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-ECATIO-001")
#epicsEnvSet(AIMOD1-AI1, "LNS-LEBT-010:VAC-VGP-00031")
#epicsEnvSet(AIMOD1-AI2, "LNS-LEBT-010:VAC-VGP-10000")
#epicsEnvSet(AIMOD1-AI3, "LNS-LEBT-010:VAC-VGP-00021")
#epicsEnvSet(AIMOD1-AI4, "LNS-LEBT-010:VAC-VGC-10000")
#epicsEnvSet(AIMOD2-AI1, "LNS-LEBT-010:VAC-VGP-00071")
#epicsEnvSet(AIMOD2-AI2, "LNS-LEBT-010:VAC-VGP-30000")
#epicsEnvSet(AIMOD2-AI3, "LNS-LEBT-010:VAC-VGP-00081")
#epicsEnvSet(AIMOD2-AI4, "LNS-LEBT-010:VAC-VGC-30000")
#epicsEnvSet(AIMOD3-AI1, "LNS-LEBT-010:VAC-VGD-10000")
#epicsEnvSet(AIMOD3-AI3, "LNS-LEBT-010:VAC-VGD-30000")
require ecatio_lns_lebt_010_vac_ecatio_001, catania
< /opt/epics/modules/ecatio_lns_lebt_010_vac_ecatio_001/catania/startup/startup.cmd
